FROM java:8

VOLUME /tmp

ADD build/libs/deploy.jar deploy.jar

RUN bash -c 'touch /deploy.jar'

EXPOSE 8880

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/deploy.jar"]