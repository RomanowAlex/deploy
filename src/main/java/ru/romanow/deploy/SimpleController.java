package ru.romanow.deploy;

import lombok.Data;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static org.slf4j.LoggerFactory.getLogger;

@Controller
public class SimpleController {
    private static final Logger logger = getLogger(SimpleController.class);

    @Value("${server.port}")
    private String port;

    @GetMapping("/ping")
    public @ResponseBody PingResponse ping() {
        logger.info("### Request ping on {} ###", port);
        return new PingResponse("OK");
    }

    @GetMapping
    public ModelAndView helloWorld(@RequestParam(value = "name", required = false, defaultValue = "world") String name, HttpServletResponse response) {
        response.setHeader(HttpHeaders.CACHE_CONTROL, "no-cache");
        logger.info("### Receive request [Hello, world] ###");
        return new ModelAndView("index", new HashMap<String, String>() {{
            put("name", name);
        }});
    }

    @GetMapping("/cache")
    @ResponseBody
    public ResponseEntity<String> cache() {
        logger.info("### Receive request [Cache] ###");
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(1, TimeUnit.MINUTES))
                .body("OK");
    }

    @Data
    private class PingResponse {
        private final String data;
    }
}
